package com.example.efectura.demer2.ui.fragment;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.model.Message;
import com.example.efectura.demer2.network.model.req.CancelTicketReq;
import com.example.efectura.demer2.ui.activity.LoginActivity;
import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.MenuAdapter;
import com.example.efectura.demer2.ui.adapter.ScrollHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainFragment extends Fragment implements ScrollHandler {

    private long mScrollToItemId = -1;
    private RecyclerView recyclerView;
    private MenuAdapter mMenuAdapter;
    private LinearLayoutManager layoutManager;
    private NestedScrollView nestedScrollView;
    private ImageView ivLogout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initRV(view,savedInstanceState);

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutDialog();
            }
        });

        setData();
        return view;
    }

    private void logoutDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = 0;
        switch (metrics.widthPixels) {
            case 360:
                height = 390;
                break;
            case 540:
                height = 585;
                break;
            case 720:
                height = 780;
                break;
            case 1080:
                height = 1170;
                break;
            case 1440:
                height = 1560;
                break;
        }

        dialog.getWindow().setLayout(metrics.widthPixels, height);

        dialog.setContentView(R.layout.dialog_custom);
        Button btnOk = dialog.findViewById(R.id.confirmButtom);
        Button btnCancel = dialog.findViewById(R.id.cancelButton);
        TextView tvMessage = dialog.findViewById(R.id.textTicket);
        tvMessage.setText(R.string.message_logout);
        btnCancel.setText(R.string.no_logout);
        btnOk.setText(R.string.yes_logout);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void initRV(View view,Bundle savedInstanceState){
        ivLogout = view.findViewById(R.id.ivLogout);
        nestedScrollView = view.findViewById(R.id.scrollView);
        nestedScrollView.setSmoothScrollingEnabled(true);

        recyclerView = view.findViewById(R.id.recyclerItem);
        recyclerView.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        mMenuAdapter = new MenuAdapter(getActivity(), savedInstanceState, this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mMenuAdapter);
    }

    private void setData(){
        int [] images = {
          R.drawable.ic_account_balance_wallet,
          R.drawable.ic_account_balance_wallet,
          R.drawable.ic_account_balance_wallet
        };
        List<String> itemList = Arrays.asList(getResources().getStringArray(R.array.item_main));
        List<Item> items = new ArrayList<>();
        for(int i = 0; i < itemList.size(); i++){
            Item item = new Item();
            item.setId(i);
            item.setTitle(itemList.get(i));
            item.setIconImg(images[i]);
            switch (i){
                case 0:{
                    item.setFirst(true);
                    item.setType("new_user");
                    break;
                }
                case 1:{
                    item.setType("top_up");
                    break;
                }
                case 2:{
                    item.setType("balance");
                    break;
                }
            }
            items.add(item);
        }
        mMenuAdapter.swapCursor(items);
        if (mScrollToItemId != -1) {
            scrollToItem(mScrollToItemId);
            setSmoothScrollStableId(-1);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMenuAdapter.saveInstance(outState);
    }

    private void scrollToItem(long itemId) {
        final int alarmCount = mMenuAdapter.getItemCount();
        int itemPosition = -1;
        for (int i = 0; i < alarmCount; i++) {
            long id = mMenuAdapter.getItemId(i);
            if (id == itemId) {
                itemPosition = i;
                break;
            }
        }
        if (itemPosition >= 0) {
            mMenuAdapter.expand(itemPosition);
            smoothScrollTo(itemPosition);
        }
    }

    @Override
    public void setSmoothScrollStableId(long stableId) {
        mScrollToItemId = stableId;
    }


    @Override
    public void smoothScrollTo(final int position) {
        nestedScrollView.post(new Runnable() {
            @Override
            public void run() {
                ObjectAnimator.ofInt(nestedScrollView, "scrollY",  recyclerView.getTop()+ position*90+80).setDuration(800).start();
            }
        });
    }
}
