package com.example.efectura.demer2.network.model.req;

import com.google.gson.annotations.SerializedName;

public class PurchaseReq {
    @SerializedName("amount")
    private int amount;
    @SerializedName("ticketType")
    private String ticketType;
    @SerializedName("phoneNumber")
    private String phoneNumber;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
