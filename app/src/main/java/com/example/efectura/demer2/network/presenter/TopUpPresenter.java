package com.example.efectura.demer2.network.presenter;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.CashinRequest;
import com.example.efectura.demer2.network.model.res.CashinResponse;
import com.example.efectura.demer2.ui.adapter.viewholder.TopUpViewHolder;
import com.example.efectura.demer2.utils.ErrorDialogHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TopUpPresenter {

    private TopUpViewHolder view;
    private ServiceApi serviceApi;

    public TopUpPresenter(TopUpViewHolder view,ServiceApi serviceApi){
        this.view = view;
        this.serviceApi = serviceApi;
    }

    public void topUp(CashinRequest req){
        view.showLoading();
        serviceApi = ServiceHelper.serverApi();
        serviceApi.topUp(req).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::topUpResponse, this::handleError);
    }

    private void topUpResponse(CashinResponse topUpResponse) {
        if(!topUpResponse.getFinancialtransactionid().equals("")){
            view.hideLoading();
            view.successDialog();
        }
    }

    private void handleError(Throwable throwable) {
        view.hideLoading();
        ErrorDialogHelper.showDialog(view.itemView.getContext(),throwable.getMessage(),
                view.itemView.getContext().getResources().getString(R.string.close));
    }
}
