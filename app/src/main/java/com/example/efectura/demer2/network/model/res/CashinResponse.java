package com.example.efectura.demer2.network.model.res;

import com.google.gson.annotations.SerializedName;

public class CashinResponse {

    @SerializedName("amountFee")
    private String amountFee;
    @SerializedName("currencyFee")
    private String currencyFee;
    @SerializedName("amountSender")
    private String amountSender;
    @SerializedName("currencySender")
    private String currencySender;
    @SerializedName("amountReceiver")
    private String amountReceiver;
    @SerializedName("currencyReceiver")
    private String currencyReceiver;
    @SerializedName("financialtransactionid")
    private String financialtransactionid;

    public String getAmountFee() {
        return amountFee;
    }

    public void setAmountFee(String amountFee) {
        this.amountFee = amountFee;
    }

    public String getCurrencyFee() {
        return currencyFee;
    }

    public void setCurrencyFee(String currencyFee) {
        this.currencyFee = currencyFee;
    }

    public String getAmountSender() {
        return amountSender;
    }

    public void setAmountSender(String amountSender) {
        this.amountSender = amountSender;
    }

    public String getCurrencySender() {
        return currencySender;
    }

    public void setCurrencySender(String currencySender) {
        this.currencySender = currencySender;
    }

    public String getAmountReceiver() {
        return amountReceiver;
    }

    public void setAmountReceiver(String amountReceiver) {
        this.amountReceiver = amountReceiver;
    }

    public String getCurrencyReceiver() {
        return currencyReceiver;
    }

    public void setCurrencyReceiver(String currencyReceiver) {
        this.currencyReceiver = currencyReceiver;
    }

    public String getFinancialtransactionid() {
        return financialtransactionid;
    }

    public void setFinancialtransactionid(String financialtransactionid) {
        this.financialtransactionid = financialtransactionid;
    }
}
