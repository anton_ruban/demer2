package com.example.efectura.demer2.network.model.req;

import com.google.gson.annotations.SerializedName;

public class RegistrationUserReq {

    @SerializedName("MsIsdn")
    private String MsIsdn;
    @SerializedName("FirstName")
    private String FirstName;
    @SerializedName("LastName")
    private String LastName;
    @SerializedName("BirthDate")
    private String BirthDate;
    @SerializedName("Lang")
    private String Lang;
    @SerializedName("OthrIdId")
    private String OthrIdId;
    @SerializedName("OthrIdPass")
    private String OthrIdPass;
    @SerializedName("Mail")
    private String Mail;
    @SerializedName("Mobile")
    private String Mobile;

    public String getMsIsdn() {
        return MsIsdn;
    }

    public void setMsIsdn(String msIsdn) {
        MsIsdn = msIsdn;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getLang() {
        return Lang;
    }

    public void setLang(String lang) {
        Lang = lang;
    }

    public String getOthrIdId() {
        return OthrIdId;
    }

    public void setOthrIdId(String othrIdId) {
        OthrIdId = othrIdId;
    }

    public String getOthrIdPass() {
        return OthrIdPass;
    }

    public void setOthrIdPass(String othrIdPass) {
        OthrIdPass = othrIdPass;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }
}
