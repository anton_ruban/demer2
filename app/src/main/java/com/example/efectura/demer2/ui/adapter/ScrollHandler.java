package com.example.efectura.demer2.ui.adapter;

public interface ScrollHandler {
    void setSmoothScrollStableId(long stableId);
    void smoothScrollTo(int position);
}
