package com.example.efectura.demer2.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.presenter.LoginPresenter;
import com.example.efectura.demer2.network.view.LoginView;
import com.example.efectura.demer2.utils.PreferenceManager;
import com.wang.avi.AVLoadingIndicatorView;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private EditText etMsisdn;
    private EditText etPincode;
    private Button btnLogin;
    private Button btnRegister;
    private Button btnActivation;
    private LinearLayout llFields;
    private LinearLayout llBtn;
    private AVLoadingIndicatorView pBar;

    private LoginPresenter presenter;
    private ServiceApi serviceApi;
    private PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);
        serviceApi = ServiceHelper.serverApi();
        presenter = new LoginPresenter(this,serviceApi);
        preferenceManager = new PreferenceManager(this);
        initView();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etMsisdn.getText().length() != 10 || etMsisdn.getText().toString().equals("")){
                    etMsisdn.setError(getResources().getString(R.string.error));
                } else if(etPincode.getText().length() != 6 || etPincode.getText().toString().equals("")) {
                    etPincode.setError(getResources().getString(R.string.error));
                } else {
                    presenter.loginUser(etMsisdn.getText().toString(),etPincode.getText().toString());
                    preferenceManager.setMsisdn(etMsisdn.getText().toString());
                }
            }
        });

        btnActivation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,ActivationActivity.class));
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
            }
        });


    }

    private void initView(){
        etMsisdn = findViewById(R.id.etMsisdn);
        etPincode = findViewById(R.id.etPincode);
        btnLogin = findViewById(R.id.loginBtn);
        btnRegister = findViewById(R.id.btnRegister);
        btnActivation = findViewById(R.id.btnActivation);
        llFields = findViewById(R.id.llFields);
        llBtn = findViewById(R.id.llBtn);
        pBar = findViewById(R.id.pBar);
    }

    @Override
    public void showLoading() {
        llFields.setVisibility(View.GONE);
        llBtn.setVisibility(View.GONE);
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    @Override
    public void hideLoading() {
        llFields.setVisibility(View.VISIBLE);
        llBtn.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.GONE);
        pBar.hide();
    }

    @Override
    public void navigateMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void navigateUserMain() {
        startActivity(new Intent(this,MainUserActivity.class));
        finish();
    }

    @Override
    public Context mContext() {
        return this;
    }
}
