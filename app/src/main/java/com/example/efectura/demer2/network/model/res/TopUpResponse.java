package com.example.efectura.demer2.network.model.res;

import com.google.gson.annotations.SerializedName;

public class TopUpResponse {

    @SerializedName("amountFee")
    private String amountFee;
    @SerializedName("currencyFee")
    private String currencyFee;
    @SerializedName("transactionid")
    private String transactionid;
    @SerializedName("amountSender")
    private String amountSender;
    @SerializedName("currencySender")
    private String currencySender;

    public String getAmountFee() {
        return amountFee;
    }

    public void setAmountFee(String amountFee) {
        this.amountFee = amountFee;
    }

    public String getCurrencyFee() {
        return currencyFee;
    }

    public void setCurrencyFee(String currencyFee) {
        this.currencyFee = currencyFee;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getAmountSender() {
        return amountSender;
    }

    public void setAmountSender(String amountSender) {
        this.amountSender = amountSender;
    }

    public String getCurrencySender() {
        return currencySender;
    }

    public void setCurrencySender(String currencySender) {
        this.currencySender = currencySender;
    }
}
