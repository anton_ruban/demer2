package com.example.efectura.demer2.ui.adapter.viewholder;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.RegistrationUserReq;
import com.example.efectura.demer2.network.presenter.RegisterUserAgentPresenter;
import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.MenuAdapter;
import com.example.efectura.demer2.utils.ErrorDialogHelper;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Calendar;

public class NewUserViewHolder extends ItemViewHolder {

    private EditText etMsisdn;
    private EditText etFirst;
    private EditText etLast;
    private EditText etEmail;
    private TextView tvBD;
    private Button btnRegistration;
    private LinearLayout llUser;
    private AVLoadingIndicatorView pBar;

    private RegisterUserAgentPresenter presenter;
    private ServiceApi serviceApi;
    private RegistrationUserReq request;
    private int mYear, mMonth, mDay;

    public NewUserViewHolder(@NonNull View itemView,MenuAdapter menuAdapter) {
        super(itemView);

        etMsisdn = itemView.findViewById(R.id.etMsisdn);
        etFirst = itemView.findViewById(R.id.etFirst);
        etLast = itemView.findViewById(R.id.etLast);
        etEmail = itemView.findViewById(R.id.etEmail);
        tvBD = itemView.findViewById(R.id.tvBD);
        btnRegistration = itemView.findViewById(R.id.btnRegister);
        llUser = itemView.findViewById(R.id.llNewUser);
        pBar = itemView.findViewById(R.id.pBar);

        serviceApi = ServiceHelper.serverApi();
        presenter = new RegisterUserAgentPresenter(this,serviceApi);
        request = new RegistrationUserReq();

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuAdapter.collapse(getAdapterPosition());
            }
        });
    }

    @Override
    public void bind(Context context, Item item) {
        bindHeader(context, item);
        bindTvBD(context, item);
        bindButton(context, item);
    }

    private void bindButton(Context context,Item item){
        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etMsisdn.getText().length() != 10 || etMsisdn.getText().toString().equals("")){
                    etMsisdn.setError(itemView.getContext().getResources().getString(R.string.error));
                } else if(etFirst.getText().toString().equals("")) {
                    etFirst.setError(itemView.getContext().getResources().getString(R.string.error));
                } else if(etLast.getText().toString().equals("")) {
                    etLast.setError(itemView.getContext().getResources().getString(R.string.error));
                } else if(etMsisdn.getText().length() != 10 || etMsisdn.getText().toString().equals("")
                        && etFirst.getText().toString().equals("")) {

                } else {
                    request = new RegistrationUserReq();
                    request.setMsIsdn(etMsisdn.getText().toString());
                    request.setFirstName(etFirst.getText().toString());
                    request.setLastName(etLast.getText().toString());
                    request.setBirthDate(tvBD.getText().toString());
                    request.setLang("EN");
                    request.setOthrIdId("AT126713HA");
                    request.setOthrIdPass("PASS");
                    request.setMail(etEmail.getText().toString());
                    if(!etMsisdn.getText().toString().equals("")){
                        request.setMobile("+52-" + etMsisdn.getText().toString().substring(2));
                        presenter.registrationUser(request);
                    } else {
                        ErrorDialogHelper.showDialog(itemView.getContext(),"Please, fill all fields",
                                itemView.getContext().getResources().getString(R.string.close));
                    }
                }
            }
        });
    }

    public void clearFields(){
        etMsisdn.getText().clear();
        etFirst.getText().clear();
        etLast.getText().clear();
        etEmail.getText().clear();
        tvBD.setText(itemView.getContext().getResources().getString(R.string.bithdate));
    }

    private void bindTvBD(Context context,Item item){
        tvBD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        tvBD.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }

    public void showLoading(){
        llUser.setVisibility(View.GONE);
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    public void hideLoading(){
        llUser.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.GONE);
        pBar.hide();
    }
}
