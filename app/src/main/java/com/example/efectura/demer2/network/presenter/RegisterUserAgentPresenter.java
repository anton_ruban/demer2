package com.example.efectura.demer2.network.presenter;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.RegistrationUserReq;
import com.example.efectura.demer2.network.model.res.BaseResponse;
import com.example.efectura.demer2.ui.adapter.viewholder.NewUserViewHolder;
import com.example.efectura.demer2.utils.ErrorDialogHelper;
import com.example.efectura.demer2.utils.SuccessDialogHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RegisterUserAgentPresenter {

    private NewUserViewHolder view;
    private ServiceApi serviceApi;

    public RegisterUserAgentPresenter(NewUserViewHolder view,ServiceApi serviceApi){
        this.view = view;
        this.serviceApi = serviceApi;
    }

    public void registrationUser(RegistrationUserReq req) {
        view.showLoading();
        serviceApi = ServiceHelper.serverApi();
        serviceApi.registrationUser(req).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::registResponse, this::handleLoginError);
    }

    private void registResponse(BaseResponse response) {
        if(response.isSuccessStatusCode() && response.getStatusCode() == 200){
            view.clearFields();
            view.hideLoading();
            SuccessDialogHelper.showDialog(view.itemView.getContext(),response.getReasonPhrase(),
                    view.itemView.getContext().getResources().getString(R.string.ok));
        }
        if(response.getStatusCode() == 500){
            view.hideLoading();
            ErrorDialogHelper.showDialog(view.itemView.getContext(), view.itemView.getContext().getResources().getString(R.string.already_register),
                    view.itemView.getContext().getResources().getString(R.string.close));
        }
    }

    private void handleLoginError(Throwable throwable) {
        view.hideLoading();
        ErrorDialogHelper.showDialog(view.itemView.getContext(),throwable.getMessage(),
                view.itemView.getContext().getResources().getString(R.string.close));
    }
}
