package com.example.efectura.demer2.network.model.req;

import com.google.gson.annotations.SerializedName;

public class ActivationReq {

    @SerializedName("Identity")
    private String Identity;
    @SerializedName("Pincode")
    private String Pincode;
    @SerializedName("Password")
    private String Password;

    public String getIdentity() {
        return Identity;
    }

    public void setIdentity(String identity) {
        Identity = identity;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
