package com.example.efectura.demer2.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.efectura.demer2.R;

public class ErrorDialogHelper {
    public static void showDialog(Context context,String message,String txtBtn){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);
        d.setContentView(R.layout.dialog_error);

        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = 0;
        switch (metrics.widthPixels) {
            case 360:
                height = 390;
                break;
            case 540:
                height = 585;
                break;
            case 720:
                height = 780;
                break;
            case 1080:
                height = 1170;
                break;
            case 1440:
                height = 1560;
                break;
        }

        d.getWindow().setLayout(metrics.widthPixels, height);
        TextView tvError = d.findViewById(R.id.tvError);
        Button btnClose = d.findViewById(R.id.btnClose);
        tvError.setText(message);
        btnClose.setText(txtBtn);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();

    }
}
