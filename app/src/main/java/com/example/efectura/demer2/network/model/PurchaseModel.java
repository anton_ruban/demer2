package com.example.efectura.demer2.network.model;

import com.google.gson.annotations.SerializedName;

public class PurchaseModel {
    @SerializedName("mcodeNumber")
    private String mcodeNumber;
    @SerializedName("mcodeMatrix")
    private String mcodeMatrix;

    public String getMcodeNumber() {
        return mcodeNumber;
    }

    public void setMcodeNumber(String mcodeNumber) {
        this.mcodeNumber = mcodeNumber;
    }

    public String getMcodeMatrix() {
        return mcodeMatrix;
    }

    public void setMcodeMatrix(String mcodeMatrix) {
        this.mcodeMatrix = mcodeMatrix;
    }
}
