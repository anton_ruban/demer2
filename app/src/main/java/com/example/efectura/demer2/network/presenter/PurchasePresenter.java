package com.example.efectura.demer2.network.presenter;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.PurchaseReq;
import com.example.efectura.demer2.network.model.res.PurchaseResponse;
import com.example.efectura.demer2.ui.adapter.user.viewholder.PurchaseTicketViewHolder;
import com.example.efectura.demer2.utils.ErrorDialogHelper;
import com.example.efectura.demer2.utils.SuccessDialogHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PurchasePresenter {

    private PurchaseTicketViewHolder view;
    private ServiceApi serviceApi;

    public PurchasePresenter(PurchaseTicketViewHolder view,ServiceApi serviceApi){
        this.view = view;
        this.serviceApi = serviceApi;
    }

    public void purchaseTicket(PurchaseReq req){
        view.showLoading();
        serviceApi = ServiceHelper.serverApi();
        serviceApi.purchaseTicket(ServiceApi.contentType,req).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::purchaseResponse, this::handlePurchaseError);
    }

    private void purchaseResponse(PurchaseResponse purchaseResponse) {
        if (purchaseResponse != null){
            view.hideLoading();
            SuccessDialogHelper.showDialog(view.itemView.getContext(),view.itemView.getContext().getResources().getString(R.string.purchaseSuccess),view.itemView.getContext().getResources().getString(R.string.ok));
        } else {
            ErrorDialogHelper.showDialog(view.itemView.getContext(),view.itemView.getContext().getResources().getString(R.string.purchaseError),view.itemView.getContext().getResources().getString(R.string.close));

        }
    }

    private void handlePurchaseError(Throwable throwable) {
        view.hideLoading();
        ErrorDialogHelper.showDialog(view.itemView.getContext(),view.itemView.getContext().getResources().getString(R.string.purchaseError),view.itemView.getContext().getResources().getString(R.string.close));
    }
}
