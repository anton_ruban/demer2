package com.example.efectura.demer2.network.model.req;

import com.google.gson.annotations.SerializedName;

public class CashinRequest {
    @SerializedName("sendFri")
    private String sendFri;
    @SerializedName("receiveFri")
    private String receiveFri;
    @SerializedName("amount")
    private String amount;
    @SerializedName("curr")
    private String curr;

    public String getSendFri() {
        return sendFri;
    }

    public void setSendFri(String sendFri) {
        this.sendFri = sendFri;
    }

    public String getReceiveFri() {
        return receiveFri;
    }

    public void setReceiveFri(String receiveFri) {
        this.receiveFri = receiveFri;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }
}
