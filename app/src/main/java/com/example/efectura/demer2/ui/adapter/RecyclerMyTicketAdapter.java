package com.example.efectura.demer2.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.response.GetAllTicketResponse;
import com.example.efectura.demer2.ui.activity.MainActivity;
import com.example.efectura.demer2.ui.activity.OneTicketActivity;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

public class RecyclerMyTicketAdapter extends RecyclerView.Adapter<RecyclerMyTicketAdapter.ViewHolder> {

    private Context context;
    private ArrayList<GetAllTicketResponse> getAllTicketResponses;

    public RecyclerMyTicketAdapter(Context context, ArrayList<GetAllTicketResponse> getAllTicketResponses) {
        this.context = context;
        this.getAllTicketResponses = getAllTicketResponses;
    }

    @NonNull
    @Override
    public RecyclerMyTicketAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_ticket, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerMyTicketAdapter.ViewHolder holder, final int position) {

        holder.codeTicket.setText(getAllTicketResponses.get(position).getMcodeMatrix());
        holder.codePhone.setText(getAllTicketResponses.get(position).getMcodeNumber());
        String[] arr=getAllTicketResponses.get(position).getExpiredTime().split(" ");
        holder.textDateValid.setText(arr[0]);
        holder.textCostNumber.setText(String.valueOf(getAllTicketResponses.get(position).getAmount()));
        if (getAllTicketResponses.get(position).getAmount() == 18) {
            holder.textTypeSGL.setText(context.getResources().getString(R.string.single));
        }
        if (getAllTicketResponses.get(position).getAmount() == 36) {
            holder.textTypeSGL.setText(context.getResources().getString(R.string.ten));
        }
        if (getAllTicketResponses.get(position).getAmount() == 180) {
            holder.textTypeSGL.setText(context.getResources().getString(R.string.fifteen));
        }
        holder.linearMyTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OneTicketActivity.class);
                intent.putExtra("codeTicket", getAllTicketResponses.get(position).getMcodeMatrix());
                intent.putExtra("codePhone", getAllTicketResponses.get(position).getMcodeNumber());
                intent.putExtra("textCost", String.valueOf(getAllTicketResponses.get(position).getAmount()));
                intent.putExtra("textTypeSGL", holder.textTypeSGL.getText().toString());
                intent.putExtra("dateExpire",holder.textDateValid.getText().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return getAllTicketResponses.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView codeTicket;
        public TextView codePhone;
        public TextView textSGL;
        public TextView textTypeSGL;
        public TextView textValid;
        public TextView textDateValid;
        public TextView textCost;
        public TextView textCostNumber;
        public AVLoadingIndicatorView aviProgressBar;
        public LinearLayout linearMyTicket;

        public ViewHolder(View view) {
            super(view);
            codeTicket = view.findViewById(R.id.codeTicket);
            codePhone = view.findViewById(R.id.codePhone);
            textSGL = view.findViewById(R.id.textSGL);
            textTypeSGL = view.findViewById(R.id.textTypeSGL);
            textValid = view.findViewById(R.id.textValid);
            textDateValid = view.findViewById(R.id.textDateValid);
            textCost = view.findViewById(R.id.textCost);
            textCostNumber = view.findViewById(R.id.textCostNumber);
            aviProgressBar = view.findViewById(R.id.aviProgressBar);
            linearMyTicket = view.findViewById(R.id.linearMyTicket);
            linearMyTicket.bringToFront();
        }
    }
}
