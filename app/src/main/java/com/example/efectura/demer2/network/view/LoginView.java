package com.example.efectura.demer2.network.view;

import android.content.Context;

public interface LoginView {
    void showLoading();
    void hideLoading();
    void navigateMain();
    void navigateUserMain();
    Context mContext();
}
