package com.example.efectura.demer2.network.model.res;


import com.google.gson.annotations.SerializedName;

public class BaseResponse {

    @SerializedName("isSuccessStatusCode")
    private boolean isSuccessStatusCode;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("reasonPhrase")
    private String reasonPhrase;

    public boolean isSuccessStatusCode() {
        return isSuccessStatusCode;
    }

    public void setSuccessStatusCode(boolean successStatusCode) {
        isSuccessStatusCode = successStatusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }

    public void setReasonPhrase(String reasonPhrase) {
        this.reasonPhrase = reasonPhrase;
    }
}
