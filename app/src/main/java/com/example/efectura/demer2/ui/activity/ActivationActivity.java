package com.example.efectura.demer2.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.ActivationReq;
import com.example.efectura.demer2.network.presenter.ActivationPresenter;
import com.example.efectura.demer2.network.view.ActivationView;
import com.wang.avi.AVLoadingIndicatorView;

public class ActivationActivity extends AppCompatActivity implements ActivationView {

    private EditText etMsisdn;
    private EditText etPincode;
    private EditText etPassword;
    private Button btnLogin;
    private LinearLayout llFields;
    private LinearLayout llBtn;
    private AVLoadingIndicatorView pBar;

    private ActivationPresenter presenter;
    private ActivationReq request;
    private ServiceApi serviceApi;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activation);
        serviceApi = ServiceHelper.serverApi();
        presenter = new ActivationPresenter(this,serviceApi);
        initView();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etMsisdn.getText().length() != 10 || etMsisdn.getText().toString().equals("")){
                    etMsisdn.setError(getResources().getString(R.string.error));
                } else if (etPincode.getText().length() != 6 || etPincode.getText().toString().equals("")) {
                    etPincode.setError(getResources().getString(R.string.error));
                } else if(etMsisdn.getText().length() != 10 && etPincode.getText().length() != 6 && etPassword.getText().length()  < 6) {
                    etMsisdn.setError(getResources().getString(R.string.error));
                    etPincode.setError(getResources().getString(R.string.error));
                    etPassword.setError(getResources().getString(R.string.error));
                } else if(etPassword.getText().length()  < 6 || etPassword.getText().toString().equals("")){
                    etPassword.setError(getResources().getString(R.string.error));
                }else {
                    request = new ActivationReq();
                    request.setIdentity(etMsisdn.getText().toString());
                    request.setPincode(etPincode.getText().toString());
                    request.setPassword(etPassword.getText().toString());
                    presenter.activationUser(request);
                }
            }
        });
    }

    private void initView(){
        etMsisdn = findViewById(R.id.etMsisdn);
        etPincode = findViewById(R.id.etPincode);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnActivation);
        llFields = findViewById(R.id.llFields);
        llBtn = findViewById(R.id.llBtn);
        pBar = findViewById(R.id.pBar);
    }

    @Override
    public void showLoading() {
        llFields.setVisibility(View.GONE);
        llBtn.setVisibility(View.GONE);
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    @Override
    public void hideLoading() {
        llFields.setVisibility(View.VISIBLE);
        llBtn.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.GONE);
        pBar.hide();
    }

    @Override
    public void navigateLogin() {
        startActivity(new Intent(this,LoginActivity.class));
    }

    @Override
    public Context mContext() {
        return this;
    }
}
