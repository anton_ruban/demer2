package com.example.efectura.demer2.ui.adapter.user.viewholder;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.PurchaseReq;
import com.example.efectura.demer2.network.presenter.PurchasePresenter;
import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.viewholder.ItemViewHolder;
import com.example.efectura.demer2.utils.PreferenceManager;
import com.wang.avi.AVLoadingIndicatorView;

public class PurchaseTicketViewHolder extends ItemViewHolder {

    public final ImageView iv18;
    public final ImageView iv36;
    public final ImageView iv180;
    public final AVLoadingIndicatorView pBar;
    public final LinearLayout llPurchase;
    private PreferenceManager preferenceManager;

    private ServiceApi serviceApi;
    private PurchasePresenter presenter;
    private PurchaseReq purchaseReq;

    public PurchaseTicketViewHolder(@NonNull View itemView, MenuUserAdapter mainUserAdapter) {
        super(itemView);

        iv18 = itemView.findViewById(R.id.ivTicket18);
        iv36 = itemView.findViewById(R.id.ivTicket36);
        iv180 = itemView.findViewById(R.id.ivTicket180);
        pBar = itemView.findViewById(R.id.pBar);
        llPurchase = itemView.findViewById(R.id.llPurchase);

        serviceApi = ServiceHelper.serverApi();
        presenter = new PurchasePresenter(this,serviceApi);
        preferenceManager = new PreferenceManager(itemView.getContext());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainUserAdapter.collapse(getAdapterPosition());
            }
        });
    }

    @Override
    public void bind(Context context, Item item) {
        bindHeader(context, item);
        bindPurchase(context, item);
    }

    public void showLoading(){
        llPurchase.setVisibility(View.GONE);
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    public void hideLoading(){
        llPurchase.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.GONE);
        pBar.hide();
    }

    private void bindPurchase(Context context, final Item item){
        purchaseReq = new PurchaseReq();
        iv18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchaseReq.setAmount(18);
                purchaseReq.setTicketType("SPSC1 A");
                purchaseReq.setPhoneNumber(preferenceManager.getMsisdn());
                dialogConfirm(purchaseReq);
            }
        });

        iv36.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchaseReq.setAmount(36);
                purchaseReq.setTicketType("SPSC2 A");
                purchaseReq.setPhoneNumber(preferenceManager.getMsisdn());
                dialogConfirm(purchaseReq);
            }
        });

        iv180.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchaseReq.setAmount(180);
                purchaseReq.setTicketType("SPSC3 A");
                purchaseReq.setPhoneNumber(preferenceManager.getMsisdn());
                dialogConfirm(purchaseReq);
            }
        });
    }

    private void dialogConfirm(final PurchaseReq req){
        final Dialog dialog = new Dialog(itemView.getContext());
        dialog.setContentView(R.layout.dialog_custom);

        Button confirmButton = dialog.findViewById(R.id.confirmButtom);
        Button cancelButton = dialog.findViewById(R.id.cancelButton);
        TextView textTicket = dialog.findViewById(R.id.textTicket);

        confirmButton.setText(itemView.getContext().getResources().getString(R.string.ok));
        cancelButton.setText(itemView.getContext().getResources().getString(R.string.close));

        if(req.getTicketType().equals("SPSC1 A")){
            textTicket.setText(itemView.getContext().getResources().getString(R.string.textSingleTrip));
        }
        if(req.getTicketType().equals("SPSC2 A")){
            textTicket.setText(itemView.getContext().getResources().getString(R.string.text10Trip));
        }
        if(req.getTicketType().equals("SPSC3 A")){
            textTicket.setText(itemView.getContext().getResources().getString(R.string.text15Trip));
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                presenter.purchaseTicket(req);
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}

