package com.example.efectura.demer2.network.api;


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class ServiceHelper {

//    private final static String BASE_URL = "http://test.efectura.com:8006/api/v0.1/Empayit/";
    private final static String BASE_URL = "http://test.efectura.com:8006/api/v0.1/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient(String base_url) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(base_url)
                    .build();
        }
        return retrofit;
    }

    public static ServiceApi serverApi(){
        return ServiceHelper.getClient(BASE_URL).create(ServiceApi.class);
    }
}
