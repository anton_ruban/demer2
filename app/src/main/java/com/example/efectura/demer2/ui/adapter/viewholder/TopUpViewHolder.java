package com.example.efectura.demer2.ui.adapter.viewholder;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.CashinRequest;
import com.example.efectura.demer2.network.presenter.TopUpPresenter;
import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.MenuAdapter;
import com.wang.avi.AVLoadingIndicatorView;

public class TopUpViewHolder extends ItemViewHolder {

    private Button btnTopUp;
    private EditText etSendFri;
    private EditText etReceiveFri;
    private EditText etCurrency;
    private EditText etAmount;
    private LinearLayout llTopUp;
    private AVLoadingIndicatorView pBar;

    private CashinRequest request;
    private ServiceApi serviceApi;
    private TopUpPresenter presenter;

    public TopUpViewHolder(@NonNull View itemView,MenuAdapter menuAdapter) {
        super(itemView);

        etSendFri = itemView.findViewById(R.id.etSendFri);
        etReceiveFri = itemView.findViewById(R.id.etReceiveFri);
        etCurrency = itemView.findViewById(R.id.etCurrency);
        etAmount = itemView.findViewById(R.id.etAmount);
        btnTopUp = itemView.findViewById(R.id.btnTopUp);
        llTopUp = itemView.findViewById(R.id.llTopUp);
        pBar = itemView.findViewById(R.id.pBar);

        serviceApi = ServiceHelper.serverApi();
        presenter = new TopUpPresenter(this,serviceApi);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFields();
                menuAdapter.collapse(getAdapterPosition());
            }
        });
    }

    @Override
    public void bind(Context context, Item item) {
        bindHeader(context, item);
        bindButton(context, item);
    }

    private void clearFields(){
        etSendFri.getText().clear();
        etReceiveFri.getText().clear();
        etAmount.getText().clear();
    }

    private void bindButton(Context context,Item item){
        btnTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                request = new CashinRequest();
                request.setSendFri(etSendFri.getText().toString());
                request.setReceiveFri(etReceiveFri.getText().toString());
                request.setAmount(etAmount.getText().toString());
                request.setCurr("MXN");
                presenter.topUp(request);
            }
        });
    }

    public void showLoading(){
        llTopUp.setVisibility(View.GONE);
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    public void hideLoading(){
        llTopUp.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.GONE);
        pBar.hide();
    }

    public void successDialog() {
        final Dialog dialog = new Dialog(itemView.getContext());
        dialog.setContentView(R.layout.dialog_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)itemView.getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = 0;
        switch (metrics.widthPixels) {
            case 360:
                height = 390;
                break;
            case 540:
                height = 585;
                break;
            case 720:
                height = 780;
                break;
            case 1080:
                height = 1170;
                break;
            case 1440:
                height = 1560;
                break;
        }

        dialog.getWindow().setLayout(metrics.widthPixels, height);
        TextView tvError = dialog.findViewById(R.id.tvSuccess);
        tvError.setText(R.string.topup_success);
        Button btnOk = dialog.findViewById(R.id.btnOK);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                clearFields();
            }
        });
        dialog.show();
    }
}
