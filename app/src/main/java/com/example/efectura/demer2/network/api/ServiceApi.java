package com.example.efectura.demer2.network.api;

import com.example.efectura.demer2.network.model.req.ActivationReq;
import com.example.efectura.demer2.network.model.req.CancelTicketReq;
import com.example.efectura.demer2.network.model.req.CashinRequest;
import com.example.efectura.demer2.network.model.req.PurchaseReq;
import com.example.efectura.demer2.network.model.req.RegistrationUserReq;
import com.example.efectura.demer2.network.model.req.TopUpReq;
import com.example.efectura.demer2.network.model.res.BalanceResponse;
import com.example.efectura.demer2.network.model.res.BaseResponse;
import com.example.efectura.demer2.network.model.res.CashinResponse;
import com.example.efectura.demer2.network.model.res.LoginResponse;
import com.example.efectura.demer2.network.model.res.PurchaseResponse;
import com.example.efectura.demer2.network.model.res.TopUpResponse;
import com.example.efectura.demer2.network.response.GetAllTicketResponse;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceApi {

    String contentType = "application/json";

    @POST("Test/AppRegistration")
    Observable<BaseResponse> registrationUser(@Body RegistrationUserReq req);

    @GET("Test/TestWalletBalanceAsync")
    Observable<BalanceResponse> getBalance(@Query("identity") String identity,
                                           @Query("pincode") String pincode,
                                           @Query("fri") String fri);

    @POST("Test/AppActivation")
    Observable<BaseResponse> activationUser(@Body ActivationReq req);

    @POST("Empayit/OrderTicket")
    Observable<PurchaseResponse> purchaseTicket(@Header("Content-type") String type, @Body PurchaseReq req);

    @POST("Empayit/CancellTicket")
    Observable<String> cancelTicket(@Header("Content-type") String type, @Body CancelTicketReq req);

    @POST("Test/TestWalletCashinAsync")
    Observable<CashinResponse> topUp(@Body CashinRequest req);

    @GET("Test/TestWalletLoginAsync")
    Observable<LoginResponse> login(@Query("identity") String identity, @Query("pincode") String pincode);

    @POST("Empayit/GetAllTickets")
    Call<ArrayList<GetAllTicketResponse>> getAllTickets(@Header("Content-type") String type, @Body String phoneNumber);
}
