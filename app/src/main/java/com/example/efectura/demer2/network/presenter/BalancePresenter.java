package com.example.efectura.demer2.network.presenter;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.res.BalanceResponse;
import com.example.efectura.demer2.ui.adapter.viewholder.CheckBalanceViewHolder;
import com.example.efectura.demer2.utils.ErrorDialogHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BalancePresenter {

    private CheckBalanceViewHolder view;
    private ServiceApi serviceApi;

    public BalancePresenter(CheckBalanceViewHolder view,ServiceApi serviceApi){
        this.view = view;
        this.serviceApi = serviceApi;
    }

    public void getBalance(String identity,String pincode,String fri){
        view.showLoading();
        serviceApi = ServiceHelper.serverApi();
        serviceApi.getBalance(identity, pincode, fri).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::balanceResponse, this::handlerErrorBalance);
    }

    private void balanceResponse(BalanceResponse balanceResponse) {
        if(balanceResponse != null){
            view.receiveBalance(balanceResponse);
        }
    }

    private void handlerErrorBalance(Throwable throwable) {
        view.hideLoading();
        ErrorDialogHelper.showDialog(view.itemView.getContext(),throwable.getMessage(),
                view.itemView.getContext().getResources().getString(R.string.close));
    }
}
