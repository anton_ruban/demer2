package com.example.efectura.demer2.ui.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.ScrollHandler;
import com.example.efectura.demer2.ui.adapter.viewholder.CheckBalanceViewHolder;
import com.example.efectura.demer2.ui.adapter.viewholder.ItemCollapsedViewHolder;
import com.example.efectura.demer2.ui.adapter.viewholder.ItemViewHolder;
import com.example.efectura.demer2.ui.adapter.viewholder.NewUserViewHolder;
import com.example.efectura.demer2.ui.adapter.viewholder.TopUpViewHolder;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<ItemViewHolder>{

    private static final String KEY_EXPANDED_ID = "expandedId";

    private List<Item> itemList;
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final ScrollHandler mScrollHandler;
    private int mExpandedPosition = RecyclerView.NO_POSITION;
    private long mExpandedId = RecyclerView.NO_ID;

    private static final int NEW_USER_HOLDER = R.layout.card_menu_new_user;
    private static final int TOPUP_HOLDER = R.layout.card_menu_topup;
    private static final int BALABCE_HOLDER = R.layout.card_menu_balance;
    private static final int COLLAPSED_HOLDER = R.layout.card_menu_collapsed;

    public MenuAdapter(Context context, Bundle savedState, ScrollHandler smoothScrollController) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mScrollHandler = smoothScrollController;
        if (savedState != null) {
            mExpandedId = savedState.getLong(KEY_EXPANDED_ID, -1);
        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v = mInflater.inflate(viewType, parent, false /* attachToRoot */);
        switch (viewType){
            case NEW_USER_HOLDER: return new NewUserViewHolder(v, this);
            case TOPUP_HOLDER : return  new TopUpViewHolder(v,this);
            case BALABCE_HOLDER : return  new CheckBalanceViewHolder(v,this);
            default: return new ItemCollapsedViewHolder(v, this);
        }
    }

    @Override
    public int getItemViewType(int position) {
        final long stableId = getItemId(position);

        if(stableId != RecyclerView.NO_ID && stableId == mExpandedId){
            switch (itemList.get(position).getType()){
                case "new_user": return NEW_USER_HOLDER;
                case "top_up": return TOPUP_HOLDER;
                case "balance": return BALABCE_HOLDER;

                default: return BALABCE_HOLDER;
            }
        } else return COLLAPSED_HOLDER;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {
        itemViewHolder.bind(mContext, itemList.get(position));
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }


    @Override
    public long getItemId(int position) {
        if (itemList == null) {
            return -1;
        } else
            return itemList.get(position).getId();
    }

    public void saveInstance(Bundle outState) {
        outState.putLong(KEY_EXPANDED_ID, mExpandedId);
    }

    public void expand(int position) {
        final long stableId = getItemId(position);
        if (mExpandedId == stableId) {
            return;
        }
        mExpandedId = stableId;
        mScrollHandler.smoothScrollTo(position);

        if (mExpandedPosition >= 0) {
            notifyItemChanged(mExpandedPosition);
        }
        mExpandedPosition = position;
        notifyItemChanged(position);
    }

    public void collapse(int position) {
        mExpandedPosition = RecyclerView.NO_POSITION;
        mExpandedId = RecyclerView.NO_ID;
        notifyItemChanged(position);
    }

    public void swapCursor(List<Item> list) {
        if (itemList == list) {
            return;
        }
        itemList = list;
        notifyDataSetChanged();
    }
}
