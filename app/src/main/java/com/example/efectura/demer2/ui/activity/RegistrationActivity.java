package com.example.efectura.demer2.ui.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.RegistrationUserReq;
import com.example.efectura.demer2.network.presenter.RegistrationUserPresenter;
import com.example.efectura.demer2.network.view.RegistrationUserView;
import com.example.efectura.demer2.utils.ErrorDialogHelper;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Calendar;

public class RegistrationActivity extends AppCompatActivity implements RegistrationUserView {

    private EditText etMsisdn;
    private EditText etFirstName;
    private EditText edLastName;
    private EditText etEmail;
    private TextView tvBithDate;
    private Button btnRegistration;
    private int mYear, mMonth, mDay;
    private RegistrationUserReq request;
    private RegistrationUserPresenter presenter;
    private ServiceApi serviceApi;
    private AVLoadingIndicatorView pBar;
    private LinearLayout linear;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_registration_user);
        serviceApi = ServiceHelper.serverApi();
        presenter = new RegistrationUserPresenter(this,serviceApi);

        initView();

        tvBithDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bithDatePicker();
            }
        });

        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etMsisdn.getText().length() != 10 || etMsisdn.getText().toString().equals("")){
                    etMsisdn.setError(getResources().getString(R.string.error));
                } else if(etFirstName.getText().toString().equals("")) {
                    etFirstName.setError(getResources().getString(R.string.error));
                } else if(edLastName.getText().toString().equals("")) {
                    edLastName.setError(getResources().getString(R.string.error));
                } else if(etMsisdn.getText().length() != 10 || etMsisdn.getText().toString().equals("")
                        && etFirstName.getText().toString().equals("")) {

                } else {
                    request = new RegistrationUserReq();
                    request.setMsIsdn(etMsisdn.getText().toString());
                    request.setFirstName(etFirstName.getText().toString());
                    request.setLastName(edLastName.getText().toString());
                    request.setBirthDate(tvBithDate.getText().toString());
                    request.setLang("EN");
                    request.setOthrIdId("AT126713HA");
                    request.setOthrIdPass("PASS");
                    request.setMail(etEmail.getText().toString());
                    if(!etMsisdn.getText().toString().equals("")){
                        request.setMobile("+52-" + etMsisdn.getText().toString().substring(2));
                        presenter.registrationUser(request);
                    } else {
                        ErrorDialogHelper.showDialog(RegistrationActivity.this,"Please, fill all fields",getString(R.string.close));
                    }
                }
            }
        });
    }

    private void initView() {
        etMsisdn = findViewById(R.id.etMsisdn);
        etFirstName = findViewById(R.id.etFirstName);
        edLastName = findViewById(R.id.edLastName);
        etEmail = findViewById(R.id.etEmail);
        tvBithDate = findViewById(R.id.tvBithDate);
        btnRegistration = findViewById(R.id.btnRegistration);
        pBar = findViewById(R.id.pBar);
        linear = findViewById(R.id.linearRegister);
    }

    private void bithDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                tvBithDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void messageResponse(String message) {
        Toast.makeText(this,  message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateMain() {
        startActivity(new Intent(this,ActivationActivity.class));
    }

    @Override
    public void showLoading() {
        linear.setVisibility(View.GONE);
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    @Override
    public void hideLoading() {
        linear.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.GONE);
        pBar.hide();
    }

    @Override
    public Context mContext() {
        return this;
    }
}
