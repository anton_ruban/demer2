package com.example.efectura.demer2.ui.adapter.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.MenuAdapter;

public class ItemCollapsedViewHolder extends ItemViewHolder {

    public ItemCollapsedViewHolder(@NonNull View itemView,MenuAdapter menuAdapter) {
        super(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuAdapter.expand(getAdapterPosition());
            }
        });
    }

    @Override
    public void bind(Context context, Item item) {
        bindHeader(context, item);
        if(item.isFirst()){
            setMargins(itemView,0,0,0,0);
        } else {
            setMargins(itemView,0,-45,0,0);
        }
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}
