package com.example.efectura.demer2.network.presenter;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.ActivationReq;
import com.example.efectura.demer2.network.model.res.BaseResponse;
import com.example.efectura.demer2.network.view.ActivationView;
import com.example.efectura.demer2.utils.ErrorDialogHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ActivationPresenter {

    private ActivationView view;
    private ServiceApi serviceApi;

    public ActivationPresenter(ActivationView view,ServiceApi serviceApi){
        this.view = view;
        this.serviceApi = serviceApi;
    }

    public void activationUser(ActivationReq req){
        view.showLoading();
        serviceApi = ServiceHelper.serverApi();
        serviceApi.activationUser(req).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::registResponse, this::handleLoginError);
    }

    private void handleLoginError(Throwable throwable) {
        view.hideLoading();
        ErrorDialogHelper.showDialog(view.mContext(),throwable.getMessage(),view.mContext().getResources().getString(R.string.close));
    }

    private void registResponse(BaseResponse response) {
        if(response.isSuccessStatusCode() && response.getStatusCode() == 200){
            view.navigateLogin();
        }
    }
}
