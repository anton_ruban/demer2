package com.example.efectura.demer2.network.view;

import android.content.Context;

public interface RegistrationUserView {
    void messageResponse(String message);
    void navigateMain();
    void showLoading();
    void hideLoading();
    Context mContext();
}

