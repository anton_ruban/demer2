package com.example.efectura.demer2.ui.adapter.viewholder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.ui.adapter.Item;

public abstract class ItemViewHolder extends RecyclerView.ViewHolder {

    public final TextView tvHeader;
    public final ImageView ivHeader;

    public ItemViewHolder(@NonNull View itemView) {
        super(itemView);
        tvHeader = itemView.findViewById(R.id.tvHeader);
        ivHeader = itemView.findViewById(R.id.ivIcon);
    }

    public abstract void bind(Context context, Item item);

    protected void bindHeader(Context context,Item item){
        tvHeader.setText(item.getTitle());
        Drawable drawable = ContextCompat.getDrawable(context, item.getIconImg());
        ivHeader.setImageDrawable(drawable);
    }
}
