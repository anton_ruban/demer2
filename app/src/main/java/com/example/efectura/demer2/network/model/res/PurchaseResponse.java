package com.example.efectura.demer2.network.model.res;

import com.example.efectura.demer2.network.model.PurchaseModel;
import com.google.gson.annotations.SerializedName;

public class PurchaseResponse {
    @SerializedName("mValidateTicket")
    private PurchaseModel mValidateTicket;

    public PurchaseModel getmValidateTicket() {
        return mValidateTicket;
    }

    public void setmValidateTicket(PurchaseModel mValidateTicket) {
        this.mValidateTicket = mValidateTicket;
    }
}
