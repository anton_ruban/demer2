package com.example.efectura.demer2.network.presenter;

import android.util.Log;

import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.CancelTicketReq;
import com.example.efectura.demer2.network.view.OneTicketView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class OneTicketPresenter {

    private OneTicketView view;
    private ServiceApi serviceApi;

    public OneTicketPresenter(OneTicketView view,ServiceApi serviceApi){
        this.view = view;
        this.serviceApi = serviceApi;
    }

    public void cancelTicket(CancelTicketReq req){
        view.showLoading();
        serviceApi = ServiceHelper.serverApi();
        serviceApi.cancelTicket(ServiceApi.contentType,req).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::cancelResponse, this::handleError);
    }

    private void cancelResponse(String s) {
        Log.d("response",s);
        view.hideLoading();
        view.successDialog();
    }

    private void handleError(Throwable throwable) {
        view.hideLoading();
        view.errorDialog();
    }
}
