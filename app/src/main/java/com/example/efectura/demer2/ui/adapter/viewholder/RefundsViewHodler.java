package com.example.efectura.demer2.ui.adapter.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.MenuAdapter;

public class RefundsViewHodler extends ItemViewHolder {

    public RefundsViewHodler(@NonNull View itemView,MenuAdapter menuAdapter) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuAdapter.collapse(getAdapterPosition());
            }
        });
    }

    @Override
    public void bind(Context context, Item item) {
        bindHeader(context, item);
    }
}
