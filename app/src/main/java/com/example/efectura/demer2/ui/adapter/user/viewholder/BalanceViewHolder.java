package com.example.efectura.demer2.ui.adapter.user.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.res.BalanceResponse;
import com.example.efectura.demer2.network.presenter.BalancePresenter;
import com.example.efectura.demer2.network.presenter.BalanceUserPresenter;
import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.MenuAdapter;
import com.example.efectura.demer2.ui.adapter.viewholder.ItemViewHolder;
import com.wang.avi.AVLoadingIndicatorView;

public class BalanceViewHolder extends ItemViewHolder {

    private EditText etIdentity;
    private EditText etPincode;
    private EditText etFri;
    private Button btnBalance;
    private TextView tvAmount;
    private TextView tvCurrency;
    private LinearLayout llBalance;
    private AVLoadingIndicatorView pBar;
    private LinearLayout llFinalBalance;

    private BalanceUserPresenter presenter;
    private ServiceApi serviceApi;

    public BalanceViewHolder(@NonNull View itemView, MenuUserAdapter menuAdapter) {
        super(itemView);

        etIdentity = itemView.findViewById(R.id.etIdentity);
        etPincode = itemView.findViewById(R.id.etPincode);
        etFri = itemView.findViewById(R.id.etFri);
        llBalance = itemView.findViewById(R.id.llBalance);
        btnBalance = itemView.findViewById(R.id.btnGetBalance);
        pBar = itemView.findViewById(R.id.pBar);
        llFinalBalance = itemView.findViewById(R.id.llFinalBalance);
        tvAmount = itemView.findViewById(R.id.tvAmount);
        tvCurrency = itemView.findViewById(R.id.tvCurrency);

        serviceApi = ServiceHelper.serverApi();
        presenter = new BalanceUserPresenter(this,serviceApi);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFields();
                llBalance.setVisibility(View.VISIBLE);
                llFinalBalance.setVisibility(View.GONE);
                menuAdapter.collapse(getAdapterPosition());
            }
        });
    }

    @Override
    public void bind(Context context, Item item) {
        bindHeader(context, item);
        bindButton(context, item);
    }

    private void bindButton(Context context,Item item){
        btnBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etIdentity.getText().length() != 10 && etIdentity.getText().toString().equals("")){
                    etIdentity.setError(itemView.getContext().getResources().getString(R.string.error));
                } else if(etPincode.getText().length() != 10 && etPincode.getText().toString().equals("")){
                    etPincode.setError(itemView.getContext().getResources().getString(R.string.error));
                } else  if(etFri.getText().length() != 10 && etFri.getText().toString().equals("")){
                    etFri.setError(itemView.getContext().getResources().getString(R.string.error));
                } else {
                    presenter.getBalance(etIdentity.getText().toString(),
                            etPincode.getText().toString(),
                            etFri.getText().toString());
                }
            }
        });
    }

    private void clearFields(){
        etIdentity.getText().clear();
        etFri.getText().clear();
        etPincode.getText().clear();
    }

    public void receiveBalance(BalanceResponse response){
        pBar.setVisibility(View.VISIBLE);
        pBar.hide();
        llFinalBalance.setVisibility(View.VISIBLE);
        tvAmount.setText(String.valueOf(response.getAmount()));
        tvCurrency.setText(response.getCurrency());
    }

    public void showLoading(){
        llBalance.setVisibility(View.GONE);
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    public void hideLoading(){
        llBalance.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.GONE);
        pBar.hide();
    }
}
