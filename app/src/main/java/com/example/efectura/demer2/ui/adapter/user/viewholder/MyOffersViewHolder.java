package com.example.efectura.demer2.ui.adapter.user.viewholder;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.PurchaseReq;
import com.example.efectura.demer2.network.presenter.OffersPresenter;
import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.viewholder.ItemViewHolder;
import com.example.efectura.demer2.utils.PreferenceManager;
import com.wang.avi.AVLoadingIndicatorView;

public class MyOffersViewHolder extends ItemViewHolder {

    public final ImageView iv69;
    public final ImageView iv73;
    public final AVLoadingIndicatorView pBar;
    public final LinearLayout llOffers;

    private OffersPresenter presenter;
    private ServiceApi serviceApi;
    private PurchaseReq purchaseReq;
    private PreferenceManager preferenceManager;

    public MyOffersViewHolder(@NonNull View itemView, MenuUserAdapter mainUserAdapter) {
        super(itemView);
        preferenceManager = new PreferenceManager(itemView.getContext());

        iv69 = itemView.findViewById(R.id.iv69);
        iv73 = itemView.findViewById(R.id.iv73);
        pBar = itemView.findViewById(R.id.pBar);
        llOffers = itemView.findViewById(R.id.llOffers);

        serviceApi = ServiceHelper.serverApi();
        presenter = new OffersPresenter(this,serviceApi);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainUserAdapter.collapse(getAdapterPosition());
            }
        });
    }

    @Override
    public void bind(Context context, Item item) {
        bindHeader(context, item);
        purchaseOffers(context,item);
    }

    private void purchaseOffers(Context context, Item item){
        purchaseReq = new PurchaseReq();
        iv69.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchaseReq.setAmount(69);
                purchaseReq.setTicketType("Loyalty");
                purchaseReq.setPhoneNumber(preferenceManager.getMsisdn());
                dialogConfirm(purchaseReq);
            }
        });
        iv73.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchaseReq.setAmount(73);
                purchaseReq.setTicketType("Burger");
                purchaseReq.setPhoneNumber(preferenceManager.getMsisdn());
                dialogConfirm(purchaseReq);
            }
        });
    }

    private void dialogConfirm(final PurchaseReq req){
        final Dialog dialog = new Dialog(itemView.getContext());
        dialog.setContentView(R.layout.dialog_custom);

        Button confirmButton = dialog.findViewById(R.id.confirmButtom);
        Button cancelButton = dialog.findViewById(R.id.cancelButton);
        TextView textTicket = dialog.findViewById(R.id.textTicket);

        confirmButton.setText(itemView.getContext().getResources().getString(R.string.ok));
        cancelButton.setText(itemView.getContext().getResources().getString(R.string.close));

        if(req.getTicketType().equals("Loyalty")){
            textTicket.setText(itemView.getContext().getResources().getString(R.string.layaltyText));
        }
        if(req.getTicketType().equals("Burger")){
            textTicket.setText(itemView.getContext().getResources().getString(R.string.burgerText));
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                presenter.purchaseOffers(req);
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showLoading(){
        llOffers.setVisibility(View.GONE);
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    public void hideLoading(){
        llOffers.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.GONE);
        pBar.hide();
    }
}

