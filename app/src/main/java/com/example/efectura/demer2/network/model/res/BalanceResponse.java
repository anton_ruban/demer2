package com.example.efectura.demer2.network.model.res;

import com.google.gson.annotations.SerializedName;

public class BalanceResponse {

    @SerializedName("amount")
    private String amount;
    @SerializedName("currency")
    private String currency;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
