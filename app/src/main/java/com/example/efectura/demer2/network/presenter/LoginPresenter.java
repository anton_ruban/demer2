package com.example.efectura.demer2.network.presenter;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.ActivationReq;
import com.example.efectura.demer2.network.model.res.BaseResponse;
import com.example.efectura.demer2.network.model.res.LoginResponse;
import com.example.efectura.demer2.network.view.LoginView;
import com.example.efectura.demer2.utils.ErrorDialogHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenter {

    private LoginView view;
    private ServiceApi serviceApi;

    public LoginPresenter(LoginView view,ServiceApi serviceApi){
        this.view = view;
        this.serviceApi = serviceApi;
    }

    public void loginUser(String identity,String pincode){
        view.showLoading();
        serviceApi = ServiceHelper.serverApi();
        serviceApi.login(identity,pincode).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::loginResponse, this::handleLoginError);
    }

    private void loginResponse(LoginResponse response) {
        if(response != null){
            if(response.isAgent()){
                view.navigateMain();
            } else {
                view.navigateUserMain();
            }
        }
    }

    private void handleLoginError(Throwable throwable) {
        view.hideLoading();
        ErrorDialogHelper.showDialog(view.mContext(),throwable.getMessage(),view.mContext().getResources().getString(R.string.close));

    }
}
