package com.example.efectura.demer2.network.model;

import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("mCode")
    private String mCode;

    public String getmCode() {
        return mCode;
    }

    public void setmCode(String mCode) {
        this.mCode = mCode;
    }
}
