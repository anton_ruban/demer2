package com.example.efectura.demer2.network.model.res;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("result")
    private String result;
    @SerializedName("isAgent")
    private boolean isAgent;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isAgent() {
        return isAgent;
    }

    public void setAgent(boolean agent) {
        isAgent = agent;
    }
}
