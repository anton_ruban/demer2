package com.example.efectura.demer2.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    int PRIVATE_MODE = 0;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context mContext;

    private static final String PREF_NAME = "PreferenceManager.com.example.efectura.demer2";
    private static final String MSISDN = "PreferenceManager.MSISDN";
    private static final String PINCODE = "PreferenceManager.PINCODE";

    public PreferenceManager(Context context){
        this.mContext = context;
        pref = mContext.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setMsisdn(String msisdn){
        editor.putString(MSISDN,msisdn);
        editor.commit();
    }

    public String getMsisdn(){
        return pref.getString(MSISDN,"");
    }

    public void setPincode(String pincode){
        editor.putString(PINCODE,pincode);
        editor.commit();
    }

    public String getPINCODE() {
        return pref.getString(PINCODE,"");
    }
}
