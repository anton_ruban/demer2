package com.example.efectura.demer2.network.presenter;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.req.RegistrationUserReq;
import com.example.efectura.demer2.network.model.res.BaseResponse;
import com.example.efectura.demer2.network.view.RegistrationUserView;
import com.example.efectura.demer2.utils.ErrorDialogHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RegistrationUserPresenter {

    private RegistrationUserView view;
    private ServiceApi serviceApi;

    public RegistrationUserPresenter(RegistrationUserView view, ServiceApi serviceApi) {
        this.view = view;
        this.serviceApi = serviceApi;
    }

    public void registrationUser(RegistrationUserReq req) {
        view.showLoading();
        serviceApi = ServiceHelper.serverApi();
        serviceApi.registrationUser(req).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::registResponse, this::handleLoginError);
    }

    private void handleLoginError(Throwable throwable) {
        view.hideLoading();
        ErrorDialogHelper.showDialog(view.mContext(), throwable.getMessage(), view.mContext().getResources().getString(R.string.close));
    }

    private void registResponse(BaseResponse res) {
        if (res.isSuccessStatusCode() && res.getStatusCode() == 200) {
            view.hideLoading();
            view.navigateMain();
        }
        if (res.getStatusCode() == 500) {
            view.hideLoading();
            ErrorDialogHelper.showDialog(view.mContext(), view.mContext().getResources().getString(R.string.already_register),
                    view.mContext().getResources().getString(R.string.close));
        }
    }
}
