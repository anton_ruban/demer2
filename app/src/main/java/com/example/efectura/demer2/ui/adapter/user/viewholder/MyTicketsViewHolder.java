package com.example.efectura.demer2.ui.adapter.user.viewholder;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.response.GetAllTicketResponse;
import com.example.efectura.demer2.ui.adapter.Item;
import com.example.efectura.demer2.ui.adapter.RecyclerMyTicketAdapter;
import com.example.efectura.demer2.ui.adapter.viewholder.ItemViewHolder;
import com.example.efectura.demer2.utils.PreferenceManager;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTicketsViewHolder extends ItemViewHolder implements RecyclerView.OnClickListener {

    public final TextView itemHeader;
    private final LinearLayout linearLayoutViewPager;
    private final ImageView imgSmile;
    private final RecyclerView recyclerMyTicket;
    private final ImageView arrowLeft;
    private final ImageView arrowRight;
    public final AVLoadingIndicatorView aviProgressBar;

    private ServiceApi serviceApi;
    public ArrayList<GetAllTicketResponse> getAllTicketResponses = new ArrayList<>();;
    private RecyclerMyTicketAdapter adapter;
    private PreferenceManager preferenceManager;

    public MyTicketsViewHolder(View itemView, final MenuUserAdapter menuAdapter) {
        super(itemView);

        preferenceManager = new PreferenceManager(itemView.getContext());

        itemHeader = itemView.findViewById(R.id.textViewHeader);
        recyclerMyTicket = itemView.findViewById(R.id.recyclerMyTicket);
        aviProgressBar = itemView.findViewById(R.id.aviProgressBarTicket);
        linearLayoutViewPager = itemView.findViewById(R.id.linearLayoutViewPager);
        arrowLeft = itemView.findViewById(R.id.arrowLeft);
        arrowRight = itemView.findViewById(R.id.arrowRight);
        imgSmile = itemView.findViewById(R.id.imgSmile);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrowRight.setVisibility(View.GONE);
                arrowLeft.setVisibility(View.GONE);
                linearViewPagerHide();
                smileImageHide();
                menuAdapter.collapse(getAdapterPosition());
            }
        });
        serviceApi = ServiceHelper.serverApi();
        arrowRight.setVisibility(View.GONE);
        arrowLeft.setVisibility(View.GONE);

    }

    @Override
    public void bind(Context context, Item item) {
        bindHeader(context, item);
        getAllTicket();
    }

    private void bindRecycler(ArrayList<GetAllTicketResponse> list) {
        if (getAllTicketResponses.size() == 0) {
            arrowRight.setVisibility(View.GONE);
            arrowLeft.setVisibility(View.GONE);
        } else {
            arrowRight.setVisibility(View.VISIBLE);
            arrowLeft.setVisibility(View.VISIBLE);
        }
        final LinearLayoutManager layoutManager = new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerMyTicket.setLayoutManager(layoutManager);
        adapter = new RecyclerMyTicketAdapter(itemView.getContext(), list);
        recyclerMyTicket.setAdapter(adapter);

        arrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerMyTicket.getLayoutManager().scrollToPosition(layoutManager.findLastVisibleItemPosition() - 1);
            }
        });
        arrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerMyTicket.getLayoutManager().scrollToPosition(layoutManager.findFirstVisibleItemPosition() + 1);
            }
        });
    }

    public void getAllTicket() {
        if (getAllTicketResponses.size() != 0) {
            getAllTicketResponses.clear();
        }
        showProgressBar();
        String phoneNumber = preferenceManager.getMsisdn();
        serviceApi.getAllTickets(ServiceApi.contentType, phoneNumber).enqueue(new Callback<ArrayList<GetAllTicketResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GetAllTicketResponse>> call, Response<ArrayList<GetAllTicketResponse>> response) {
                getAllTicketResponses = response.body();
                if (response.code() == 200 || response.isSuccessful()) {
                    if (getAllTicketResponses.size() != 0) {
                        hideProgressBar();
                        linearViewPagerShow();
                        bindRecycler(getAllTicketResponses);
                        smileImageHide();
                        arrowLeft.setVisibility(View.VISIBLE);
                        arrowRight.setVisibility(View.VISIBLE);
                    } else {
                        hideProgressBar();
                        linearViewPagerHide();
                        smileImageShow();
                    }
                } else {
                    hideProgressBar();
                    linearViewPagerHide();
                    smileImageShow();
                    errorDialog();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetAllTicketResponse>> call, Throwable t) {
                hideProgressBar();
                linearViewPagerHide();
                smileImageShow();
                badConnectionDialog();
            }
        });
    }

    public void linearViewPagerShow() {
        linearLayoutViewPager.setVisibility(View.VISIBLE);
    }

    public void linearViewPagerHide() {
        linearLayoutViewPager.setVisibility(View.GONE);
    }

    public void smileImageShow() {
        imgSmile.setVisibility(View.VISIBLE);
    }

    public void smileImageHide() {
        imgSmile.setVisibility(View.GONE);
    }

    public void showProgressBar() {
        aviProgressBar.setVisibility(View.VISIBLE);
        aviProgressBar.show();
    }

    public void hideProgressBar() {
        aviProgressBar.hide();
        aviProgressBar.setVisibility(View.GONE);
    }

    public void errorDialog() {
        final Dialog dialog = new Dialog(itemView.getContext());
        dialog.setContentView(R.layout.top_up_error);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Button btnOk = dialog.findViewById(R.id.btnClose);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void badConnectionDialog() {
        final Dialog dialog = new Dialog(itemView.getContext());
        dialog.setContentView(R.layout.top_up_error);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Button btnOk = dialog.findViewById(R.id.btnClose);
        TextView textError = dialog.findViewById(R.id.textVIewError);
        textError.setText(itemView.getContext().getResources().getString(R.string.badConnectio));
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {

    }
}







