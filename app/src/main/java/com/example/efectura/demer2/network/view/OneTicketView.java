package com.example.efectura.demer2.network.view;

public interface OneTicketView {

    void showLoading();
    void hideLoading();
    void navigateMain();
    void errorDialog();
    void successDialog();
}
