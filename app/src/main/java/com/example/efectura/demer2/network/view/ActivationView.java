package com.example.efectura.demer2.network.view;

import android.content.Context;

public interface ActivationView {
    void showLoading();
    void hideLoading();
    void navigateLogin();
    Context mContext();
}
