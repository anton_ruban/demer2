package com.example.efectura.demer2.network.model.req;

import com.example.efectura.demer2.network.model.Message;
import com.google.gson.annotations.SerializedName;

public class CancelTicketReq {

    @SerializedName("clientId")
    private String clientId;
    @SerializedName("hash")
    private String hash;
    @SerializedName("messageService")
    private String messageService;
    @SerializedName("messageType")
    private String messageType;
    @SerializedName("synchronous")
    private Boolean synchronous;
    @SerializedName("message")
    private Message message;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getMessageService() {
        return messageService;
    }

    public void setMessageService(String messageService) {
        this.messageService = messageService;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Boolean getSynchronous() {
        return synchronous;
    }

    public void setSynchronous(Boolean synchronous) {
        this.synchronous = synchronous;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
