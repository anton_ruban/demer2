package com.example.efectura.demer2.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.efectura.demer2.R;
import com.example.efectura.demer2.network.api.ServiceApi;
import com.example.efectura.demer2.network.api.ServiceHelper;
import com.example.efectura.demer2.network.model.Message;
import com.example.efectura.demer2.network.model.req.CancelTicketReq;
import com.example.efectura.demer2.network.presenter.OneTicketPresenter;
import com.example.efectura.demer2.network.view.OneTicketView;
import com.wang.avi.AVLoadingIndicatorView;

public class OneTicketActivity extends AppCompatActivity implements OneTicketView {

    private OneTicketPresenter presenter;
    private ServiceApi serviceApi;
    private TextView tvCode;
    private TextView tvSGL;
    private TextView tvDate;
    private TextView tvCost;
    private TextView tvCodePhone;
    private ConstraintLayout content;
    private AVLoadingIndicatorView pBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_ticket);

        serviceApi = ServiceHelper.serverApi();
        presenter = new OneTicketPresenter(this,serviceApi);

        initView();
        setData();

        content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog();
            }
        });
    }

    private void setData(){
        tvCode.setText(getIntent().getStringExtra("codeTicket"));
        tvCodePhone.setText(getIntent().getStringExtra("codePhone"));
        tvCost.setText(getIntent().getStringExtra("textCost"));
        tvSGL.setText(getIntent().getStringExtra("textTypeSGL"));
        tvDate.setText(getIntent().getStringExtra("dateExpire"));
    }

    private void initView(){
        tvCode = findViewById(R.id.tvCode);
        tvSGL = findViewById(R.id.tvSGLType);
        tvDate = findViewById(R.id.tvValideDate);
        tvCost = findViewById(R.id.tvCost);
        tvCodePhone = findViewById(R.id.tvCodePhone);
        content = findViewById(R.id.content);
        pBar = findViewById(R.id.pBar);
        pBar.bringToFront();
    }

    private void deleteDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = 0;
        switch (metrics.widthPixels) {
            case 360:
                height = 390;
                break;
            case 540:
                height = 585;
                break;
            case 720:
                height = 780;
                break;
            case 1080:
                height = 1170;
                break;
            case 1440:
                height = 1560;
                break;
        }

        dialog.getWindow().setLayout(metrics.widthPixels, height);

        dialog.setContentView(R.layout.dialog_custom);
        Button btnOk = dialog.findViewById(R.id.confirmButtom);
        Button btnCancel = dialog.findViewById(R.id.cancelButton);
        TextView tvMessage = dialog.findViewById(R.id.textTicket);
        tvMessage.setText(R.string.cancelTicket);
        btnCancel.setText(R.string.back);
        btnOk.setText(R.string.delete);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message messageReq = new Message();
                CancelTicketReq req = new CancelTicketReq();
                messageReq.setmCode(tvCodePhone.getText().toString());
                req.setMessage(messageReq);
                presenter.cancelTicket(req);
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void showLoading() {
        pBar.setVisibility(View.VISIBLE);
        pBar.show();
    }

    @Override
    public void hideLoading() {
        pBar.setVisibility(View.GONE);
        pBar.show();
    }

    @Override
    public void navigateMain() {
        startActivity(new Intent(this,MainUserActivity.class));
        finish();
    }

    @Override
    public void errorDialog() {
        hideLoading();
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_error);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = 0;
        switch (metrics.widthPixels) {
            case 360:
                height = 390;
                break;
            case 540:
                height = 585;
                break;
            case 720:
                height = 780;
                break;
            case 1080:
                height = 1170;
                break;
            case 1440:
                height = 1560;
                break;
        }

        dialog.getWindow().setLayout(metrics.widthPixels, height);
        TextView tvError = dialog.findViewById(R.id.tvError);
        tvError.setText(R.string.purchaseError);
        Button btnOk = dialog.findViewById(R.id.btnClose);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pBar.hide();
                pBar.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void successDialog() {
        hideLoading();
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = 0;
        switch (metrics.widthPixels) {
            case 360:
                height = 390;
                break;
            case 540:
                height = 585;
                break;
            case 720:
                height = 780;
                break;
            case 1080:
                height = 1170;
                break;
            case 1440:
                height = 1560;
                break;
        }

        dialog.getWindow().setLayout(metrics.widthPixels, height);
        TextView tvError = dialog.findViewById(R.id.tvSuccess);
        tvError.setText(R.string.purchaseSuccess);
        Button btnOk = dialog.findViewById(R.id.btnOK);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateMain();
            }
        });
        dialog.show();
    }
}
