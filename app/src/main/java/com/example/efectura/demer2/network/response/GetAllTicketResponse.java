package com.example.efectura.demer2.network.response;


import com.google.gson.annotations.SerializedName;

public class GetAllTicketResponse {

    @SerializedName("id")
    private int id;
    @SerializedName("customerId")
    private int customerId;
    @SerializedName("ticketId")
    private int ticketId;
    @SerializedName("amount")
    private int amount;
    @SerializedName("amountUsed")
    private int amountUsed;
    @SerializedName("mcodeNumber")
    private String mcodeNumber;
    @SerializedName("mcodeMatrix")
    private String mcodeMatrix;
    @SerializedName("expiredTime")
    private String expiredTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmountUsed() {
        return amountUsed;
    }

    public void setAmountUsed(int amountUsed) {
        this.amountUsed = amountUsed;
    }

    public String getMcodeNumber() {
        return mcodeNumber;
    }

    public void setMcodeNumber(String mcodeNumber) {
        this.mcodeNumber = mcodeNumber;
    }

    public String getMcodeMatrix() {
        return mcodeMatrix;
    }

    public void setMcodeMatrix(String mcodeMatrix) {
        this.mcodeMatrix = mcodeMatrix;
    }

    public String getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }
}